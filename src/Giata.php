<?php

namespace Dhe\Giata;

class Giata
{
    public function greet(String $sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
}